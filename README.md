Simple Scan
==

Program to just simply scan and either save or print the result. Supports
radical b/w post processing of the image for printing. Sets scanner options
hardcoded to A4, 300dpi and 8bit. Has image preview and 4 buttons.

The repo contains a Makefile to brute force a [debian
package](simplescan_1.0_all.deb). This is ugly but simpler than learning the
current state of package creation of debian.


